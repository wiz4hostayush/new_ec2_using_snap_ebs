aws_region  = "region"
aws_profile = "aws_set_profile"
key_name    = "ec2_key_pair"

#Variables to Create Volume using Snapshot
vol_size = 10

#Variables to Create AMI using Snapshot
ami_name = "ami_name"

#Create a new EC2 instance
instance_id = "instance_to_work_on"
instance_private_key = "local_path_key_pair"
instance_type = "machine_type"


# Null Resource to execute the script
instance_user = "ec2-user"
device_name = "/dev/sdh"
folder_name = "path_to_attach_volume"