data "aws_region" "current" {}

data "aws_instance" "current" {
  instance_id = var.instance_id
  tags = {
    "Name" = "Server",
    "Type" = "main"
  }
}

resource "aws_ebs_snapshot" "example_snapshot_1" {
  depends_on = [
    data.aws_instance.current
  ]
  volume_id = data.aws_instance.current.root_block_device.*.volume_id[0]
  tags = {
    Name = "HelloWorld_snap_1"
  }
}

resource "aws_ebs_snapshot" "example_snapshot_2" {
  depends_on = [
    data.aws_instance.current
  ]
  volume_id = data.aws_instance.current.ebs_block_device.*.volume_id[0]
  tags = {
    Name = "HelloWorld_snap_2"
  }
}

resource "aws_ebs_volume" "Second-Volume" {
  depends_on = [
    aws_ebs_snapshot.example_snapshot_2
  ]
  availability_zone = "${data.aws_region.current.name}a"
  snapshot_id       = aws_ebs_snapshot.example_snapshot_2.id
  size              = var.vol_size

  tags = {
    Name = "Second-Volume"
  }
}

resource "aws_ami" "my_new_ami" {
  depends_on = [
    aws_ebs_snapshot.example_snapshot_1
  ]
  name                = var.ami_name
  virtualization_type = "hvm"
  root_device_name    = "/dev/xvda"

  ebs_block_device {
    device_name = "/dev/xvda"
    snapshot_id = aws_ebs_snapshot.example_snapshot_1.id
    # volume_size = 8
  }
  tags = {
    Name = "My-Snap-ami"
  }
}

resource "aws_instance" "new_instance" {
  depends_on = [
    aws_ami.my_new_ami
  ]
  ami               = aws_ami.my_new_ami.id
  availability_zone = "${data.aws_region.current.name}a"
  key_name          = var.key_name
  instance_type     = var.instance_type

  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_volume_attachment" "Second-Volume-att" {
  depends_on = [
    aws_instance.new_instance
  ]
  device_name = var.device_name
  volume_id   = aws_ebs_volume.Second-Volume.id
  instance_id = aws_instance.new_instance.id
}

resource "null_resource" "mount_volume" {
  depends_on = [
    aws_volume_attachment.Second-Volume-att
  ]
  triggers = {
    cluster_instance_ids = join(",", aws_instance.new_instance.*.id)
  }
  connection {
    type        = "ssh"
    user        = var.instance_user
    private_key = file(var.instance_private_key)
    host        = element(aws_instance.new_instance.*.public_ip, 0)
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mkdir ${var.folder_name}",
      "sudo resize2fs ${var.device_name}",
      "sudo mount ${var.device_name} ${var.folder_name}",
      "sudo chown -R ${var.instance_user}:${var.instance_user} ${var.folder_name}",
      "sudo df -h"
    ]
  }

}
