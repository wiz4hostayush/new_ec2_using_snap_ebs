output "Instance_info" {
    value = "${data.aws_instance.current}"
}

output "Instance_id" {
    value = "${data.aws_instance.current.id}"
}

output "root_volume_id" {
    value = "${data.aws_instance.current.root_block_device.*.volume_id[0]}"
}

output "ebs_volume_id" {
    value = "${data.aws_instance.current.ebs_block_device.*.volume_id[0]}"
}

  