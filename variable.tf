variable "aws_region"{
    description = "AWS Region"
    type = string
}

variable "aws_profile" {
    description = "AWS Profile"
    type = string
}


#Instance Variables
variable "instance_id" {
  type        = string
  description = "Instance ID"
}

variable "key_name" {
  type        = string
  description = "EC2 key name"
}

variable "instance_private_key" {
  type        = string
  description = "Instance Private Key"
}

variable "instance_type" {
  type        = string
  description = "Instance Type"
}


#Volume Variables
variable "vol_size" {
  type        = number
  description = "Volume Size"
}


#AMI Variables
variable "ami_name" {
  type        = string
  description = "AMI Name"
}

#Null Resource
variable "instance_user" {
  type        = string
  description = "Instance User"
}

variable "device_name" {
  type        = string
  description = "Device Name"
}

variable "folder_name" {
  type        = string
  description = "Folder Name"
}